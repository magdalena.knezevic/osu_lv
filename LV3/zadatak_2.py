import pandas as pd
import matplotlib . pyplot as plt

data = pd.read_csv('LV3/data_C02_emission.csv')

#a)
data['CO2 Emissions (g/km)'] . plot ( kind = 'hist' , bins = 20 )
plt.show()


#b)
data["Fuel color"] = data["Fuel Type"].map({'Z': 1, 'X': 2, 'D': 3, 'E': 4, 'N': 5})

data.plot.scatter(x ='Fuel Consumption City (L/100km)', y = 'CO2 Emissions (g/km)', c = 'Fuel color', cmap = "hot")
plt.show()

#c)
data.boxplot (column = ['Fuel Consumption Hwy (L/100km)'] , by = 'Fuel Type')
plt.show()

#d)
grouped = data.groupby('Fuel Type')
plt.subplot(1, 2, 1)
grouped.size().plot(kind="bar")


#e)
grouped2 = data.groupby('Cylinders')
plt.subplot(1, 2, 2)
grouped2['CO2 Emissions (g/km)'].plot(kind="bar")
plt.show()