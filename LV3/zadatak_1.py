import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('LV3/data_C02_emission.csv')


# a)

print("Number of rows:", len(data))
print(data.dtypes)
print("Number of missing values:", data.isnull().sum())

data.drop_duplicates()
data = data.reset_index(drop=True)
print("Number of rows after cleaning:", len(data))

data["Make"] = data["Make"].astype("category")
data["Model"] = data["Model"].astype("category")
data["Vehicle Class"] = data["Vehicle Class"].astype("category")
data["Transmission"] = data["Transmission"].astype("category")
data["Fuel Type"] = data["Fuel Type"].astype("category")

print(data.dtypes)


# b)
three_biggest = data.nlargest(3, "Fuel Consumption City (L/100km)")[["Make", "Model", "Fuel Consumption City (L/100km)"]]
print(three_biggest)
three_smallest = data.nsmallest(3, "Fuel Consumption City (L/100km)")[["Make", "Model", "Fuel Consumption City (L/100km)"]]
print(three_smallest)


# c)
vehicles_with_specific_engine_size = data[(data['Engine Size (L)'] >= 2.5) &
                                          (data['Engine Size (L)'] <= 3.5)]
number_of_vehilces = len(vehicles_with_specific_engine_size)
print("Number of vehicles with engine size between 2.5 and 3.5 L:", number_of_vehilces)

average_CO2_emission = vehicles_with_specific_engine_size["CO2 Emissions (g/km)"].mean()
print("Average CO2 emission for vehicles with engine size between 2.5 and 3.5 Liters:", average_CO2_emission)

#d)
audi_vehicles = data[(data['Make'] == "Audi")]
number_of_audi_vehicles = len(audi_vehicles)
print("Number of Audi Vehicles:", number_of_audi_vehicles)
audi_with_four_cylinders = audi_vehicles[(audi_vehicles['Cylinders'] == 4)]
average_emission_audi_with_4_cylinders = audi_with_four_cylinders["CO2 Emissions (g/km)"].mean()
print("Average emission for Audi vehicles with four cylinders:", average_emission_audi_with_4_cylinders)

#e)

new_data = data.groupby("Cylinders")
mean = new_data["CO2 Emissions (g/km)"].mean()
print(new_data.size())
print(mean)

#f)

average_diesel = data.loc[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()
average_benzin = data.loc[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].mean()
print('Prosječna gradska potrošnja dizelskih vozila:', average_diesel)
print('Prosječna gradska potrošnja vozila na benzin:', average_benzin)

median_diesel = data.loc[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].median()
median_benzin = data.loc[data['Fuel Type'] =='X']['Fuel Consumption City (L/100km)'].median()
print('Medijana gradske potrošnje dizelskih vozila:', median_diesel)
print('Medijana gradske potrošnje vozila na benzin:', median_benzin)


# g)

diesel_cars_4_cylinders = data.loc[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
diesel_cars_4_cylinders_sorted = diesel_cars_4_cylinders.sort_values(by='Fuel Consumption City (L/100km)', ascending=False)
print(diesel_cars_4_cylinders_sorted.head(1)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])


# h)
manual_cars_count = data['Transmission'].str.startswith('M').sum()
print(f'Broj vozila s ručnim mjenjačem: {manual_cars_count}')


#i)
print(data.corr(numeric_only=True))
