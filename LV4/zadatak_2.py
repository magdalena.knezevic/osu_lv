from sklearn import datasets
import numpy as np
import pandas as pd
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error, r2_score, max_error


data = pd.read_csv('LV4/data_C02_emission.csv')


data = data.drop(["Make", "Model"], axis=1)

ohe = OneHotEncoder()

input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Engine Size (L)',
                   'Cylinders',
                   'Fuel Type']

output_variables = ['CO2 Emissions (g/km)']
X = data[input_variables]
y = data[output_variables].to_numpy()
X_encoded = ohe.fit_transform(X[['Fuel Type']]).toarray()

X_train, X_test, y_train, y_test = train_test_split (X_encoded, y, test_size = 0.2, random_state = 1)

linearModel = lm.LinearRegression()
linearModel.fit( X_train , y_train )

y_test_p = linearModel.predict(X_test)

MSE = mean_squared_error(y_test, y_test_p)
RMSE = np.sqrt(MSE)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print("MSE:", MSE, "\nRMSE:", RMSE, "\nMAE:", MAE, "\nMAPE:", MAPE, "\nR2:", R2)


error = np.abs(y_test - y_test_p)
max_err = np.argmax(error)
data = pd.read_csv('LV4/data_C02_emission.csv')
max_error_model = data.iloc[max_err, :]
print(max_error_model)


