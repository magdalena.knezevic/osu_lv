from sklearn import datasets
import numpy as np
import pandas as pd
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error, r2_score


data = pd.read_csv('LV4/data_C02_emission.csv')

#a)

data = data.drop(["Make", "Model"], axis=1)

input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Engine Size (L)',
                   'Cylinders']

output_variables = ['CO2 Emissions (g/km)']
X = data[input_variables]
y = data[output_variables]

X_train, X_test, y_train, y_test = train_test_split (X, y, test_size = 0.2, random_state = 1)

#b)
plt.scatter(X_train['Fuel Consumption Hwy (L/100km)'], y_train, c='blue', s=1) #da radimo s numpy arrayem: X_train[:, i] - odabiremo sve redove samo jednog stupca
plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test, c='red', s=1)
plt.show()



#c)
sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)
X_train_n_df = pd.DataFrame(X_train_n, columns=X.columns)

plt.subplot(1, 2, 1)
plt.hist(X_train['Fuel Consumption Hwy (L/100km)'], bins=30, color='b')
plt.title("Normal")

plt.subplot(1, 2, 2)
plt.hist(X_train_n_df['Fuel Consumption Hwy (L/100km)'], bins=30, color='r')
plt.title("Standardized")

plt.show()


#d)
linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )
print(f"intercept: {linearModel.intercept_}") #vraca skalar, predstavlja b0
print(f"slope: {linearModel.coef_}") #vraca array, predstavlja ostale koeficijente(b1, b2,...)


#e)
y_test_p = linearModel.predict(X_test_n)
#plt.scatter(y_test, y_test_p, c='blue', s=1)
plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test,  s=1, c='r', label='Real')
plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test_p,  s=1, c='g', label='Predicted')

plt.show()

#f)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = np.sqrt(MSE)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print("MSE:", MSE, "\nRMSE:", RMSE, "\nMAE:", MAE, "\nMAPE:", MAPE, "\nR2:", R2)
