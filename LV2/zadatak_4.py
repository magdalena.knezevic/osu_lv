import matplotlib.pyplot as plt
import numpy as np

black = np.zeros((50, 50, 3), dtype = np.uint8)
black = 255*black

white = np.ones((50, 50, 3), dtype = np.uint8)
white = 255*white

img_left = np.vstack((black, white))
img_right = np.vstack((white, black))

img = np.hstack((img_left, img_right))

plt.figure()
plt.imshow(img)
plt.show()


