import numpy as np
import matplotlib.pyplot as plt
from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
from matplotlib import transforms


img = plt.imread ( "LV2/road.jpg" )
img = img [ : ,: ,0 ].copy()

print (img.shape )
print (img.dtype )
plt.figure()
plt.imshow( img , cmap = "gray" )
plt.show()


#a)
brightened_img = img + 30
plt.figure()
plt.imshow( brightened_img , cmap = "gray" )
plt.show()


#b)
height, width = img.shape

section_width = int(width/4)
width_cutoff_1 = section_width
width_cutoff_2 = section_width*2
width_cutoff_3 = section_width*3

s1 = img[:, :width_cutoff_1]
s2 = img[:, width_cutoff_1:width_cutoff_2]
s3 = img[:, width_cutoff_2:width_cutoff_3]
s4 = img[:, width_cutoff_3:]

plt.figure()
plt.imshow( s3 , cmap = "gray" )
plt.show()

#c)
img_rot = np.rot90(img, 3)
plt.figure()
plt.imshow(  img_rot, cmap = "gray" )
plt.show()

#d)
img_flip = np.flipud(img)
plt.figure()
plt.imshow(  img_flip, cmap = "gray" )
plt.show()
