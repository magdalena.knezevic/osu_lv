import numpy as np
import matplotlib . pyplot as plt
from matplotlib.patches import Polygon

coord = [[1.0,1.0], [3.0,1.0], [3.0,2.0], [2.0,2.0]]
coord.append(coord[0]) #repeat the first point to create a 'closed loop'

x, y = zip(*coord) #create lists of x and y values

#plt.axis([0.0, 4.0, 0.0, 4.0])
plt.figure()
plt.plot(x,y,'r', linewidth=1, marker="o", markersize=10) 
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel('x os')
plt.ylabel('y os')
plt.show() 

