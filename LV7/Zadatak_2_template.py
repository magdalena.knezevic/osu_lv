import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans


# ucitaj sliku
img = Image.imread("LV7/imgs/test_1.jpg")


# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()


# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255


# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()


number_of_colors = len(np.unique(img_array_aprox, axis=0))
print('Number of colors in original picture:', number_of_colors)


km = KMeans(n_clusters=5, init='random', n_init=5, random_state=0)


km.fit(img_array_aprox)


labels = km.predict(img_array_aprox)

print(km.cluster_centers_)
print(labels)

def recreate_image(codebook, labels, w, h):
    return codebook[labels].reshape(w, h, -1)

plt.figure()
plt.clf()
plt.axis("off")
plt.title(f"Quantized image (5 colors, K-Means)")
plt.imshow(recreate_image(km.cluster_centers_, labels, w, h))



#Elbow method
inertia_list = []
for num_clusters in range(1, 11):
    kmeans_model = KMeans(n_clusters=num_clusters, init="k-means++")
    kmeans_model.fit(img_array_aprox)
    inertia_list.append(kmeans_model.inertia_)

# plot the inertia curve
plt.plot(range(1,11),inertia_list)
plt.scatter(range(1,11),inertia_list)
plt.xlabel("Number of Clusters", size=13)
plt.ylabel("Inertia Value", size=13)
plt.title("Different Inertia Values for Different Number of Clusters", size=17)
plt.show()


#plot the elements of one cluster group
label = labels == 5
plt.figure()
plt.imshow(np.reshape(label, (h,w)))
plt.show()

