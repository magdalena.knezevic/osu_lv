song = open('song.txt', 'r')
#file_content = text.read()
#text.close()

song_dict = dict()

for line in song:
    line = line.strip()
    line = line.lower()
    line = line.replace("'s,", "")
    words = line.split(" ")
    for word in words:
        if word in song_dict:
            song_dict[word] = song_dict[word] + 1
        else:
            song_dict[word] = 1
  
count=0
for key in list(song_dict.keys()):
    print(key, ":", song_dict[key])
    if song_dict[key]==1:
        count +=1

print("Number of words that are repeating only once is: "+ str(count))




