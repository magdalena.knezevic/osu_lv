file = open("SMSSpamCollection.txt")

ham_msgs = 0
spam_msgs = 0
total_ham_words = 0
total_spam_words = 0
spam_exclamation = 0

for line in file:
    line = line.strip()

    if line.startswith('ham'):
        ham_msgs += 1
        total_ham_words += len(line.split())
    
    elif line.startswith('spam'):
        spam_msgs += 1
        total_spam_words += len(line.split())
        
        if line.endswith('!'):
            spam_exclamation += 1

avg_ham_words = total_ham_words / ham_msgs
avg_spam_words = total_spam_words / spam_msgs

print("Average number of words in ham messages: {:.2f}".format(avg_ham_words))
print("Averahe number of words in spam messages: {:.2f}".format(avg_spam_words))
print("Number of spam messages that end with exclamation mark: {}".format(spam_exclamation))
