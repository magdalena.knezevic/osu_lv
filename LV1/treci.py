from statistics import mean

list = []

while True:
    try:
        print("Enter number:")
        number = input()
        if number == 'Done':
            break
        list.append(int(number))
    except ValueError:
        print("Input error !")
    

print(list)
print("Number of elements in list: " + str(len(list)))
print("Average value of elements in list: " + str(mean(list)))
print("Max value in the list is:" + str(max(list)))
print("Min value in the list is:" + str(min(list)))