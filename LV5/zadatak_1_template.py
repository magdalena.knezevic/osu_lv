import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, classification_report

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


# a)

xmin, xmax = -5, 5
ymin, ymax = -5, 5


xd = np.array([xmin, xmax])
yd = np.array([ymin, ymax])

colors = ['blue', 'red']

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=ListedColormap(colors))
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, cmap=ListedColormap(colors), marker='x')


plt.ylim(ymin, ymax)
plt.ylabel(r'$x_2$')
plt.xlabel(r'$x_1$')
plt.show()

# b)

LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)


# c)
intercept = LogRegression_model.intercept_
slope = LogRegression_model.coef_


c = -intercept/slope[0, 1]
m = -slope[0, 0]/slope[0, 1]
yyd = m*xd + c

plt.plot(xd, yyd, linestyle='--')
plt.fill_between(xd, yyd, ymin, color='red', alpha=0.2)  # 1
plt.fill_between(xd, yyd, ymax, color='blue', alpha=0.2)  # 0


plt.scatter(X_train[:, 0], X_train[:, 1], s=8, alpha=0.5, c=y_train, cmap=ListedColormap(colors))
plt.scatter(X_test[:, 0], X_test[:, 1], s=8, alpha=0.5, c=y_test, cmap=ListedColormap(colors), marker='x')
plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)

plt.ylim(ymin, ymax)
plt.ylabel(r'$x_2$')
plt.xlabel(r'$x_1$')
plt.show()



# d)

y_test_pred = LogRegression_model.predict(X_test)

print("Tocnost: ", accuracy_score(y_test, y_test_pred))
# matrica zabune
cm = confusion_matrix(y_test, y_test_pred)
print("Matrica zabune: ", cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_pred))
disp.plot()
plt.show()
# report
print(classification_report(y_test, y_test_pred))

# e)
new_colors = ['black', 'greeen']
plt.scatter(X_test[:,0], X_test[:,1], c=y_test==y_test_pred, cmap=ListedColormap(colors), marker='x')
plt.ylabel(r'$x_2$')
plt.xlabel(r'$x_1$')

plt.show()

